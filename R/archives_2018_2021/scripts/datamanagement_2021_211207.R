#########################################
#
# Exercice : Manipulation des fichiers INSEE + vote + vaccination
# Objetifs :
# - data management
# TODO : 
#
# Date de création : kddkd
# Date de dernière modification :
# Auteur : 
#
#########################################

## Chargement des bibliothèques
library(dplyr)
library(stringr)
library(readxl)

## Chemins
path_raw_data = "/media/ant/data_crypt/bases_nationales/ressources/data"
path_export_data = "/media/ant/data_crypt/01_projets/bases_nationales/votes_vaccination/data"

list.files(path_raw_data)
list.files(path_export_data)

# Variables
codes_lyon = c("69381", "69382", "69383", "69384", "69385", "69386", "69387", "69388", "69389")
codes_marseille = c("13201", "13202", "13203", "13204", "13205", "13206", "13207", "13208", "13209",
                    "13210", "13211", "13212", "13213", "13214", "13215", "13216")
codes_paris = c("75101", "75102", "75103", "75104", "75105", "75106", "75107", "75108", "75109", "75110",
                "75111", "75112", "75113", "75114", "75115", "75116", "75117", "75118", "75119", "75120")
codes_postaux_arrondissements = c(codes_lyon, codes_marseille, codes_paris)

# Fonctions

taux = function(numerateur, denominateur, precision = 2){
  return(round(numerateur / denominateur * 100, precision))
}

## Notes

# 20 juin 2021 pour le 1er tour
# 27 juin 2021 pour le second tour

# 1) Chargement des données
##########################################################################
# https://gitlab.univ-lille.fr/master_dss/datalib/

# Vaccination
# chargement du fichier avec read.csv2

vaccination_src = read.csv2(file.path(path_raw_data, "donnees-de-vaccination-par-epci.csv"),
                            stringsAsFactors = FALSE,
                            dec = ".")
str(vaccination_src)
head(vaccination_src)

vaccination_src %>% filter(libelle_epci == 'CA SAINT LOUIS AGGLOMERATION' & semaine_injection == '2021-24')

# Votes
# resultats-par-commune-correctif_2021.csv
votes_1er_tour_src = read.csv2(file.path(path_raw_data, "resultats-par-commune-correctif_2021.csv"),
                               sep = ",", 
                               stringsAsFactors = FALSE, 
                               dec = ".")

str(votes_1er_tour_src)
head(votes_1er_tour_src)

# Population
# BTT_TD_POP1B_2017.csv
pop_age_src =  read.csv2(file.path(path_raw_data,"BTT_TD_POP1B_2017.csv"),
                         stringsAsFactors = FALSE, 
                         dec = ".") # Population et age par ville en 2017
str(pop_age_src)
head(pop_age_src)

# Pauvreté
# cc_filosofi_2017_DEP.csv

# RSA
# RSACOM2017.csv

# Diplômes
# base-cc-diplomes-formation-2017.csv

# CSP  
# TCRD_005.xlsx
#csp_src = readxl::read_xlsx(file.path(path_raw_data, "TCRD_005.xlsx"), sheet = 'DEP')

# Correspondances communes 2021
correspondance_communes_epci_src = read.csv2(file.path(path_raw_data, 'correspondance_intercommunalite_codegeo_01012021.csv'), 
                                             stringsAsFactors = FALSE)

# Correspondances communes 2017 
# correspondance_intercommunalite_codegeo_2017_01012021.csv

# 2) Datamanagement
##########################################################################

# Correspondance communes epci
# 2021
colnames(correspondance_communes_epci_src) = tolower(colnames(correspondance_communes_epci_src))
correspondance_communes_epci = correspondance_communes_epci_src %>% 
  select(codgeo, libgeo, epci, libepci)

# Vaccination
# ----------------------------------------------------------------------

vaccination = vaccination_src %>%
  # Filtre sur 'TOUT_AGE', semaines 24 et 25
  filter(classe_age == 'TOUT_AGE',
         semaine_injection %in% c('2021-24', '2021-25')) %>%
  # Sélection des colonnes 
  select(epci, libelle_epci, population_carto, semaine_injection, effectif_cumu_1_inj, effectif_cumu_termine, 
         taux_cumu_1_inj, taux_cumu_termine) %>%
  # Calcul du taux cumulé 1 injection, taux cumulé 2 injections
  mutate(taux_cumu_1_inj = round(taux_cumu_1_inj*100, 2),
         taux_cumu_termine = round(taux_cumu_termine*100, 2))

# 1er tour
vaccination_1er_tour = vaccination %>% 
  dplyr::filter(semaine_injection == '2021-24') %>%
  dplyr::rename_with(.cols = semaine_injection:taux_cumu_termine, function(x){paste0(x, "_t1")})

# 2ème tour
vaccination_2eme_tour = vaccination %>% 
  dplyr::filter(semaine_injection == '2021-25') %>%
  dplyr::rename_with(.cols = semaine_injection:taux_cumu_termine, function(x){paste0(x, "_t2")})

# 1er + 2ème tour
vaccination_epci = merge(vaccination_1er_tour, vaccination_2eme_tour,
                         by = c('epci', 'libelle_epci', 'population_carto'),
                         all = TRUE)

str(vaccination_epci)
c(nrow(vaccination_1er_tour), nrow(vaccination_2eme_tour), nrow(vaccination_epci))

# Votes
# ----------------------------------------------------------------------

pats <- c("ZA|ZB|ZC|ZD")

# Sélection : region_code, region_name, commune_code, commune_name, num_tour, 
# inscrits_nb, abstention_nb, abstention_pourc)
votes_communes_1er_tour = votes_1er_tour_src %>%
  select(region_code, region_name, commune_code, commune_name, num_tour, 
         inscrits_nb, abstention_nb, abstention_pourc) %>%
  mutate(commune_code_src = commune_code) %>%
  distinct() %>%
  # Filtrer AR en 6 et 7 ème lettre du code commune
  filter(substr(commune_code_src, 6,7) != 'AR') %>%
  # Si le nombre de caractères du code commune = 4, rajouter un 0
  mutate(
    commune_code = ifelse(nchar(commune_code_src) == 4, 
                          paste0("0", commune_code_src), 
                          commune_code_src),
    commune_code = stringr::str_replace_all(commune_code, pats, '97')
  )

str(votes_communes_1er_tour)
head(votes_communes_1er_tour)


# Merge avec EPCI
votes_epci_merge = votes_communes_1er_tour %>%
  merge(correspondance_communes_epci, 
        by.x = "commune_code", 
        by.y = "codgeo",
        all.x = TRUE)
str(votes_epci)

votes_epci_merge %>% filter(is.na(epci))

votes_epci = votes_epci_merge %>%
  rename(epci_votes = epci, lib_epci_votes = libepci) %>%
  group_by(epci_votes, lib_epci_votes) %>%
  summarize(inscrits_nb = sum(inscrits_nb, na.rm = TRUE),
            abstention_nb = sum(abstention_nb, na.rm = TRUE)) %>%
  mutate(abstention_pourc = round(abstention_nb / inscrits_nb * 100, 1))

# Votes sans EPCI
votes_epci_merge %>% 
  filter(is.na(epci_votes)) %>%
  write.csv2(file.path(path_export_data, "votes_sans_corresp.csv"),
             row.names = FALSE)

# Population
# ----------------------------------------------------------------------

colnames(pop_age_src) = tolower(colnames(pop_age_src))

pop_age_ville = pop_age_src %>%
  # Retirer les arrondissements
  filter(!codgeo %in% codes_postaux_arrondissements) %>%
  # Calculer le département, somme de l'âge
  mutate(
    departement = substr(codgeo, 1, 2),
    somme_age = nb*aged100
  ) %>% 
  # Regrouper par code geo
  group_by(codgeo) %>%
  # pour calculer :   # nb_habitants, nb_hommes, nb_femmes, somme_age
  summarise(
    departement = min(departement, na.rm = TRUE),
    nb_habitants = round(sum(nb,na.rm=TRUE)),
    nb_hommes = round(sum(nb[sexe == 1], na.rm = TRUE)),
    nb_femmes = round(sum(nb[sexe == 2], na.rm = TRUE)),
    somme_age = round(sum(somme_age, na.rm = TRUE)),
  ) %>%
  # Fusion avec correspondance epci
  merge(correspondance_communes_epci %>% select(codgeo, libgeo, epci, libepci), 
        by.x = "codgeo", 
        by.y = "codgeo",
        all.x = TRUE)

# Villes sans epci
s1 = pop_age_ville %>% filter(is.na(epci))

pop_age_epci = pop_age_ville %>%
  # Regroupement par epci
  group_by(epci) %>%
  summarize(
    departement = min(departement),
    nb_habitants = sum(nb_habitants, na.rm = TRUE),
    somme_age = sum(somme_age, na.rm = TRUE),
    nb_hommes = sum(nb_hommes, na.rm = TRUE),
    nb_femmes = sum(nb_femmes, na.rm = TRUE),
    taux_hommes_vivants = taux(nb_hommes, nb_hommes + nb_femmes),
    age_moyen_vivant = round((somme_age / nb_habitants), 2)
  )

# Pauvreté
# ----------------------------------------------------------------------

# RSA
# ----------------------------------------------------------------------

# Pour Lyon, Marseille, Paris, les données de RSA sont documentées par arrondissement (sans le total pour la ville)
# Il faut donc regrouper tous les arrondissements de ces trois villes

# Renommer nb_allocataires en nb_caf, et nb_allocataire_rsa en nb_rsa, communes en commune_rsa
# Sélectionner nb_caf, nb_rsa, codes_insee, commune_rsa

# Créer un dataframe sans arrondissement :
# filtrer les lignes pour lesquelles commune_rsa ne contient pas "ARRONDISSEMENT"

# Créer un dataframe pour Paris
# sélectionner les lignes dont codes_insee est présent dans les codes de Paris (variable codes_paris)
# Modifier le code_insee en "75056"
# Regrouper par code_insee (pour n'avoir qu'une seule ligne pour Paris)
# Somme pour nb_caf et nb_rsa
# commune_rsa = "Paris"

# Créer un dataframe pour Lyon
# sélectionner les lignes dont codes_insee est présent dans les codes de Lyon (variable codes_lyon)
# Modifier le code_insee en "69123"
# Regrouper par code_insee (pour n'avoir qu'une seule ligne pour Lyon)
# Somme pour nb_caf et nb_rsa
# commune_rsa = "Lyon"

# Créer un dataframe pour Marseille
# sélectionner les lignes dont codes_insee est présent dans les codes de Marseille (variable codes_marseille)
# Modifier le code_insee en "13055"
# Regrouper par code_insee (pour n'avoir qu'une seule ligne pour Marseille)
# Somme pour nb_caf et nb_rsa
# commune_rsa = "Marseille"

# Fusionner les 4 dataframes "sans arrondissement", Paris, Lyon, Marseille
# Merge avec la correspondance communes EPCI
# Faire la somme nb_caf et nb_rsa par EPCI

# Sortir les communes pour lesquelles il n'y a pas eu de correspondance avec EPCIAAAAAEE

# Diplôme
# ----------------------------------------------------------------------

# CSP
# ----------------------------------------------------------------------

# Join
# ----------------------------------------------------------------------

# Nombre de lignes par dataframe

data.frame(tables = c("votes", "vaccination", "pop"),
           nb_lignes = c(nrow(votes_epci), 
                         nrow(vaccination_epci),
                         nrow(pop_age_epci)))

votes_vaccination_epci = vaccination_epci %>%
  merge(votes_epci, by.x = "epci", by.y = "epci_votes", all.x = TRUE) %>%
  merge(pop_age_epci, by = "epci", all.x = TRUE)

str(votes_vaccination_epci)

plot(votes_vaccination_epci$age_moyen_vivant, votes_vaccination_epci$taux_cumu_1_inj_t1)

model = lm(taux_cumu_1_inj_t1 ~ abstention_pourc + age_moyen_vivant + taux_hommes_vivants, data = votes_vaccination_epci )
summary(model)
