#########################################
#
# Exercice : Manipulation des fichiers INSEE + vote + vaccination
# Objetifs :
# - data management
#
# Date de création : kddkd
# Date de dernière modification :
# Auteur : 
#
#########################################

## Chargement des bibliothèques
library(dplyr)
library(stringr)


## Chemins
path_import_data = "/media/ant/data_crypt/01_projets/bases_nationales/votes_vaccination/data"

list.files(path_raw_data)


# 1) Chargement des données
##########################################################################
# https://gitlab.univ-lille.fr/master_dss/datalib/

# Votes Vaccination
# chargement du fichier avec read.csv2
votes_vaccination_src = read.csv2(file.path(path_import_data, "votes_vaccinations_210913.csv"), 
                            stringsAsFactors = FALSE)

# 2) Exploration des données
##########################################################################

# Plot vaccination

# Plot votes

# Plot vaccination ~ votes

# Plot vaccination ~ âge

# Sélection des départements 59, 62, 93, et 40
# Boxplot Vaccination ~ département
# Boxplot votes ~ département


# 3) Statistiques
##########################################################################

# Tests bivariés

# Tests multivariés