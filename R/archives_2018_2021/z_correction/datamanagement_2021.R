#########################################
#
# Exercice : Manipulation des fichiers INSEE + vote + vaccination
# Objetifs :
# - data management
#
# Date de création : kddkd
# Date de dernière modification :
# Auteur : 
#
#########################################

## Chargement des bibliothèques
library(dplyr)
library(stringr)
library(readxl)

## Chemins
path_raw_data = "/media/ant/data_crypt/bases_nationales/ressources/data"
path_export_data = "/media/ant/data_crypt/01_projets/bases_nationales/votes_vaccination/data"

list.files(path_raw_data)

# Variables
codes_arrondissements_lyon = c("69381", "69382", "69383", "69384", "69385", "69386", "69387", "69388", "69389")
codes_arrondissements_marseille = c("13201", "13202", "13203", "13204", "13205", "13206", "13207", "13208", "13209",
                    "13210", "13211", "13212", "13213", "13214", "13215", "13216")
codes_arrondissements_paris = c("75101", "75102", "75103", "75104", "75105", "75106", "75107", "75108", "75109", "75110",
                "75111", "75112", "75113", "75114", "75115", "75116", "75117", "75118", "75119", "75120")
codes_arrondissements = c(codes_arrondissements_lyon, codes_arrondissements_marseille, codes_arrondissements_paris)

# Fonctions

taux = function(numerateur, denominateur, precision = 2){
  return(round(numerateur / denominateur * 100, precision))
}


## Notes

# 20 juin 2021 pour le 1er tour
# 27 juin 2021 pour le second tour

# 1) Chargement des données
##########################################################################
# https://gitlab.univ-lille.fr/master_dss/datalib/

# Vaccination
# chargement du fichier avec read.csv2
vaccination_src = read.csv2(file.path(path_raw_data, "donnees-de-vaccination-par-epci.csv"), 
                            stringsAsFactors = FALSE, 
                            dec = ".")

# Votes
# resultats-par-commune-correctif_2021.csv
votes_1er_tour_src = read.csv2(file.path(path_raw_data, "resultats-par-commune-correctif_2021.csv"),
                               sep = ",", 
                               stringsAsFactors = FALSE, 
                               dec = ".")

# Population
# BTT_TD_POP1B_2017.csv
pop_age_src =  read.csv2(file.path(path_raw_data,"BTT_TD_POP1B_2017.csv"),
                         stringsAsFactors = FALSE, 
                         dec = ".") # Population et age par ville en 2017

# Pauvreté
# cc_filosofi_2017_DEP.csv
pauvrete_src = read.csv2(file.path(path_raw_data, "cc_filosofi_2017_DEP.csv"), 
                         stringsAsFactors = FALSE, dec = ".")

# RSA
# RSACOM2017.csv
rsa_com_src =  read.csv2(file.path(path_raw_data,"RSACOM2017.csv"),
                         encoding = "UTF-8",
                         stringsAsFactors = FALSE) 

# Diplômes
# base-cc-diplomes-formation-2017.csv
dipl_form_src  = read.csv2(file.path(path_raw_data,"base-cc-diplomes-formation-2017.csv"), 
                           stringsAsFactors = FALSE, 
                           dec = ".") 

# CSP  
# TCRD_005.xlsx
csp_src = readxl::read_xlsx(file.path(path_raw_data, "TCRD_005.xlsx"), 
                            sheet = 'DEP')

# Accessibilité aux soins
accessibilite_soins_src = read_xls(file.path(path_raw_data, '76-accessibilite-aux-soins-et-attractivite-territoriale.xls'), 
                                   sheet = 2)

# Correspondance Commune -> EPCI
correspondance_communes_epci_src = read.csv2(file.path(path_raw_data, 'correspondance_intercommunalite_codegeo_01012021.csv'), 
                                             stringsAsFactors = FALSE)

# 2017
correspondance_communes_epci_2017_src = read.csv2(file.path(path_raw_data,
                                                            "correspondance_intercommunalite_codegeo_2017_01012021.csv"), 
                                                  stringsAsFactors = FALSE)

correction_epci_2017 = read.csv2(file.path("/media/ant/data_crypt/01_projets/bases_nationales/votes_vaccination/data",
                                           "correction_epci_2017.csv"), 
                                 stringsAsFactors = FALSE)

correspondance_communes_tvs_src = read.csv2(file.path(path_raw_data, 'correspondance-tvs-communes-2018-2.csv'), 
                                        sep = ",", 
                                        stringsAsFactors = FALSE)

# 2) Datamanagement
##########################################################################

# Correspondance communes epci
# 2021
colnames(correspondance_communes_epci_src) = tolower(colnames(correspondance_communes_epci_src))
correspondance_communes_epci = correspondance_communes_epci_src %>% 
  select(codgeo, libgeo, epci, libepci)

# 2017
colnames(correspondance_communes_epci_2017_src) = tolower(colnames(correspondance_communes_epci_2017_src))
correspondance_communes_epci_2017 = correspondance_communes_epci_2017_src %>% 
  select(codgeo, libgeo, epci, libepci)

# Vaccination
# ----------------------------------------------------------------------

vaccination = vaccination_src %>%
  # Filtre sur 'TOUT_AGE', semaines 24 et 25
  dplyr::filter(classe_age == 'TOUT_AGE',
         semaine_injection %in% c('2021-24', '2021-25')) %>%
  # Sélection des colonnes 
  dplyr::select(epci, libelle_epci, population_carto, semaine_injection, effectif_cumu_1_inj, effectif_cumu_termine, 
         taux_cumu_1_inj, taux_cumu_termine) %>%
  # Calcul du taux cumulé 1 injection, taux cumulé 2 injections
  dplyr::mutate(taux_cumu_1_inj = round(taux_cumu_1_inj*100, 2),
         taux_cumu_termine = round(taux_cumu_termine*100, 2))

# 1er tour
vaccination_1er_tour = vaccination %>% 
  dplyr::filter(semaine_injection == '2021-24') %>%
  dplyr::rename_with(.cols = semaine_injection:taux_cumu_termine, function(x){paste0(x, "_t1")})

# 2ème tour
vaccination_2eme_tour = vaccination %>% 
  dplyr::filter(semaine_injection == '2021-25') %>%
  dplyr::rename_with(.cols = semaine_injection:taux_cumu_termine, function(x){paste0(x, "_t2")})

# 1er + 2ème tour
vaccination_epci = merge(vaccination_1er_tour, vaccination_2eme_tour,
                    by = c('epci', 'libelle_epci', 'population_carto'),
                    all = TRUE)

str(vaccination_epci)
c(nrow(vaccination_1er_tour), nrow(vaccination_2eme_tour), nrow(vaccination_epci))

# Votes
# ----------------------------------------------------------------------

pats <- c("ZA|ZB|ZC|ZD")

votes_1er_tour = votes_1er_tour_src %>%
  select(region_code, region_name, commune_code, commune_name, num_tour, 
         inscrits_nb, abstention_nb, abstention_pourc) %>%
  mutate(commune_code_src = commune_code) %>%
  distinct() %>%
  filter(substr(commune_code_src, 6,7) != 'AR') %>%
  mutate(
    commune_code = ifelse(nchar(commune_code_src) == 4, 
                          paste0("0", commune_code_src), 
                          commune_code_src),
    commune_code = str_replace_all(commune_code, pats, '97')
  )

  renamestr(votes_1er_tour) # 34942
str(correspondance_communes_epci)

# Merge avec EPCI
votes_epci = votes_1er_tour %>%
  merge(correspondance_communes_epci %>% select(codgeo, libgeo, epci, libepci), 
        by.x = "commune_code", 
        by.y = "codgeo",
        all.x = TRUE) %>%
(epci_votes = epci, lib_epci_votes = libepci) %>%
  group_by(epci_votes, lib_epci_votes) %>%
  summarize(inscrits_nb = sum(inscrits_nb, na.rm = TRUE),
            abstention_nb = sum(abstention_nb, na.rm = TRUE)) %>%
  mutate(abstention_pourc = round(abstention_nb / inscrits_nb * 100, 1))

# Votes sans EPCI
votes_epci %>% 
  filter(is.na(epci_votes)) %>%
  write.csv2(file.path(path_export_data, "votes_sans_corresp.csv"),
             row.names = FALSE)


# Population
# ----------------------------------------------------------------------

colnames(pop_age_src) = tolower(colnames(pop_age_src))

pop_age_ville = pop_age_src %>%
  # Retirer 13055, 69123, 75056
  filter(!codgeo %in% codes_arrondissements) %>%
  # Calculer le département, somme de l'âge
  mutate(
    departement = substr(codgeo, 1, 2),
    somme_age = nb*aged100
  ) %>% 
  # Regrouper par code geo
  group_by(codgeo) %>%
  # pour calculer :   # nb_habitants, nb_hommes, nb_femmes, somme_age
  summarise(
    departement = min(departement, na.rm = TRUE),
    nb_habitants = round(sum(nb,na.rm=TRUE)),
    nb_hommes = round(sum(nb[sexe == 1], na.rm = TRUE)),
    nb_femmes = round(sum(nb[sexe == 2], na.rm = TRUE)),
    somme_age = round(sum(somme_age, na.rm = TRUE)),
  ) %>%
  # Fusion avec correspondance epci
  merge(correspondance_communes_epci %>% select(codgeo, libgeo, epci, libepci), 
        by.x = "codgeo", 
        by.y = "codgeo",
        all.x = TRUE)

# Villes sans epci
s1 = pop_age_ville %>% filter(is.na(epci))

pop_age_epci = pop_age_ville %>%
  # Regroupement par epci
  group_by(epci) %>%
  summarize(
    departement = min(departement),
    nb_habitants = sum(nb_habitants, na.rm = TRUE),
    somme_age = sum(somme_age, na.rm = TRUE),
    nb_hommes = sum(nb_hommes, na.rm = TRUE),
    nb_femmes = sum(nb_femmes, na.rm = TRUE),
    taux_hommes_vivants = taux(nb_hommes, nb_hommes + nb_femmes),
    age_moyen_vivant = round((somme_age / nb_habitants), 2)
    )


# Pauvreté
# ----------------------------------------------------------------------

pauvrete = pauvrete_src

colnames(pauvrete) = tolower(colnames(pauvrete))

pauvrete_departement = pauvrete %>%
  rename(departement = codgeo) %>%
  select(departement, tp6017)

# RSA
# ----------------------------------------------------------------------
colnames(rsa_com_src) = tolower(colnames(rsa_com_src))

rsa_1 = rsa_com_src %>%
  mutate(departement = substr(codes_insee, 1, 2)) %>%
  rename(nb_caf = nb_allocataires, 
         nb_rsa = nb_allocataire_rsa,
         commune_rsa = communes) %>%
  select(nb_caf, nb_rsa, codes_insee, commune_rsa)

# Arrondissement
rsa_arr = rsa_1 %>%
  filter(str_detect(commune_rsa, "ARRONDISSEMENT"))

# Sans arrondissement
rsa_sans_arr = rsa_1 %>%
  filter(!str_detect(commune_rsa, "ARRONDISSEMENT")) %>%
  select(codes_insee, nb_caf, nb_rsa, commune_rsa) 

# Paris
rsa_paris = rsa_1 %>%
  filter(codes_insee %in% codes_arrondissements_paris) %>%
  mutate(codes_insee = "75056") %>%
  group_by(codes_insee) %>%
  summarize(nb_caf = sum(nb_caf, na.rm = TRUE),
            nb_rsa = sum(nb_rsa, na.rm = TRUE)
  ) %>%
  mutate(commune_rsa = "Paris") 

# Lyon
rsa_lyon = rsa_1 %>%
  filter(codes_insee %in% codes_arrondissements_lyon) %>%
  mutate(codes_insee = "69123") %>%
  group_by(codes_insee) %>%
  summarize(nb_caf = sum(nb_caf, na.rm = TRUE),
            nb_rsa = sum(nb_rsa, na.rm = TRUE)
  ) %>%
  mutate(commune_rsa = "Lyon") 

# Marseille
rsa_marseille = rsa_1 %>%
  filter(codes_insee %in% codes_arrondissements_marseille) %>%
  mutate(codes_insee = "13055") %>%
  group_by(codes_insee) %>%
  summarize(nb_caf = sum(nb_caf, na.rm = TRUE),
            nb_rsa = sum(nb_rsa, na.rm = TRUE)
  ) %>%
  mutate(commune_rsa = "Marseille")

rsa_epci_merge = rbind(rsa_sans_arr, rsa_paris, rsa_marseille, rsa_lyon) %>%
  merge(correspondance_communes_epci_2017, 
        by.x = "codes_insee", 
        by.y = "codgeo",
        all.x = TRUE)  %>%
  merge(correction_epci_2017, by.x = "epci", by.y = "ancien_epci", all.x = TRUE) %>%
  mutate(epci = ifelse(!is.na(nouveau_epci), nouveau_epci, epci))


rsa_epci_merge %>% filter(!is.na(nouveau_epci))

rsa_epci_merge %>% write.csv2("/media/ant/data_crypt/01_projets/bases_nationales/votes_vaccination/data/rsa_epci.csv", row.names = FALSE)
rsa_epci_merge %>% filter(codes_insee == 59350)
rsa_epci_merge %>% filter(libgeo == 'Jardin')

rsa_epci = rsa_epci_merge %>%
  group_by(epci, libepci) %>%
  summarize(nb_caf = sum(nb_caf, na.rm = TRUE),
            nb_rsa = sum(nb_rsa, na.rm = TRUE)
  )

# Vérification merge
rsa_epci %>% filter(is.na(epci))
rsa_epci %>% filter(epci == "200050532")

# Diplôme
# ----------------------------------------------------------------------
colnames(dipl_form_src) = tolower(colnames(dipl_form_src))

dipl_form_epci_merge = dipl_form_src %>%
  # Calcul du département
  mutate(departement = substr(codgeo, 1, 2)) %>%
  filter(!codgeo %in% codes_arrondissements) %>%
  group_by(codgeo) %>%
  summarize(
    nb_sans_diplome = round(sum(p17_nscol15p_diplmin, na.rm = TRUE)),
    nb_bep_cap_bac = round(sum(p17_nscol15p_bepc + p17_nscol15p_capbep + p17_nscol15p_bac, na.rm = TRUE)),
    nb_diplome_sup = round(sum(p17_nscol15p_sup2, p17_nscol15p_sup34, p17_nscol15p_sup5, na.rm = TRUE))
  ) %>%
  merge(correspondance_communes_epci %>% select(codgeo, libgeo, epci, libepci),
        by.x = "codgeo",
        by.y = "codgeo",
        all.x = TRUE)

dipl_form_epci = dipl_form_epci_merge %>%
  group_by(epci) %>%
  summarize(nb_sans_diplome = sum(nb_sans_diplome, na.rm = TRUE),
            nb_bep_cap_bac = sum(nb_bep_cap_bac, na.rm = TRUE),
            nb_diplome_sup = sum(nb_diplome_sup, na.rm = TRUE))

dipl_form_epci_merge %>% filter(is.na(epci))

# Correspondances communes TVS
# ----------------------------------------------------------------------

correspondance_communes_tvs = correspondance_communes_tvs_src %>%
  rename(departement = 'Département',
         tvs = TVS,
         commune = Commune)

cor_lyon_marseille_paris = correspondance_communes_tvs %>% 
  dplyr::filter(between(INSEE, 75101, 75120) | between(INSEE, 13201, 13216) | between(INSEE, 69381, 69389)) %>%
  dplyr::mutate(tvs = base::gsub( " .*$", "", tvs))

correspondance_communes_tvs_epci = rbind(correspondance_communes_tvs, cor_lyon_marseille_paris) %>%
  mutate(codgeo = ifelse(between(INSEE, 75101, 75120), "75056",
                         ifelse(between(INSEE, 13201, 13216), "13055",
                                ifelse(between(INSEE, 69381, 69389), "69123", INSEE))
  )
  ) %>%
  mutate(codgeo = ifelse(is.na(codgeo), INSEE, codgeo)) %>%
  merge(correspondance_communes_epci, 
        by = "codgeo",
        all.x = TRUE)

correspondance_communes_tvs_epci %>% filter(is.na(epci))

correspondance_communes_tvs_epci %>%
  group_by(tvs) %>%
  summarize(n = n_distinct(epci))

correspondance_communes_epci_2017 %>% filter(codgeo == '01059')

# Accessibilité aux soins
# ----------------------------------------------------------------------

colnames(accessibilite_soins_src) = c('code_tv', 'lib_territoire_vie', 'type_territoire_vie', 'lib_type_territoire_vie')

accessibilite_soins = accessibilite_soins_src[3:nrow(accessibilite_soins_src),] %>%
  mutate(departement = substr(code_tv, 1, 2))

tv_ville =  accessibilite_soins %>%
  merge(correspondance_communes,
        by.x = c("lib_territoire_vie", "departement"), 
        by.y = c("tvs", "departement"), 
        all.x = TRUE)

tv_ville %>% group_by(epci, libepci) %>% summarize(n = n(),
                                                   n_d = n_distinct(lib_type_territoire_vie),
                                                   n_t = n_distinct(type_territoire_vie))

tv_ville %>% filter(epci == "200000172")

# CSP
# ----------------------------------------------------------------------

# Join
# ----------------------------------------------------------------------

# Nombre de lignes par dataframe

data.frame(tables = c("votes", "vaccination", "pop", "pauvrete_departement", "diplômes"),
           nb_lignes = c(nrow(votes_1er_tour), 
                         nrow(vaccination_epci),
                         nrow(pop_age_epci), 
                         nrow(pauvrete_departement),
                         nrow(dipl_form_epci)))

data_merge = vaccination_epci %>%
  merge(votes_1er_tour, by.x = "epci", by.y = "epci_votes", all.x = TRUE) %>%
  merge(pop_age_epci, by = "epci", all.x = TRUE) %>%
  merge(rsa_epci, by = "epci", all.x = TRUE) %>%
  mutate(prop_rsa = taux(nb_rsa, nb_habitants),
         prop_caf = taux(nb_caf, nb_habitants)
         ) %>%
  merge(dipl_form_epci, by = "epci", all.x = TRUE) %>%
  mutate(
    prop_sans_diplome = taux(nb_sans_diplome, nb_habitants),
    prop_bep_cap_bac = taux(nb_bep_cap_bac, nb_habitants),
    prop_diplome_sup = taux(nb_diplome_sup, nb_habitants)
  )

# Export

write.csv2(data_merge, file.path(path_export_data, "votes_vaccinations_211004.csv"), row.names = FALSE)
