/***********************************************************
*
* MongoDB
*
* Antoine Lamer
*
* 28/06/19
*
***********************************************************/

show dbs
show databases

show users
show roles
show profile
show logs

/* Create a collection                 */
/***************************************/

db.myNewCollection.insertOne( { x: 1 } )

/* Show collections in database  */
/*********************************/

show collections
db.getCollectionNames()

use hopital

db.myNewCollection.find({})


/* Insert one document  */
/*************************/
db.patient.insertOne(
    { patient_id : 1, first_name: "first_name_1", last_name: "last_name_1", age: 30, sex: "H" }
)

db.patient.insertOne(
    {patient_id : 2, first_name: "first_name_2", last_name: "last_name_2", age: 45, height:170, weight:74 }
)

db.patient.insertOne(
    {patient_id : 3, first_name: "first_name_3", last_name: "last_name_3", age: 45, height:170, weight:74,
        address : { zipcode: "59000", city: "Lille" } 
        }
)

/* Insert many documents */
/*************************/

db.patient.insertMany([
    { patient_id : 4, first_name: "first_name_4", last_name: "last_name_3", age: 67, height:172, weight:68},
    { patient_id : 5, first_name: "first_name_5", last_name: "last_name_5", sex: "H", age: 34, height:181, weight:80,
        address : { zipcode: "59000", city: "Lille" } },
    { patient_id : 6, first_name: "first_name_6", last_name: "last_name_6", sex: "F", age: 36, height:178, weight:80,
        address : { zipcode: "59100", city: "Roubaix" } },
    { patient_id : 7, first_name: "first_name_7", last_name: "last_name_7", sex: "F", age: 32, height:171, weight:60,
        address : { zipcode: "59113", city: "Seclin" } },
    { patient_id : 8, first_name: "first_name_8", last_name: "last_name_8", sex: "H", age: 32, height:175, weight:69,
        address : { zipcode: "59200", city: "Sourcoing" } }
    ] )


/* Update one document  */
/************************************/

db.patient.updateOne(
  { patient_id : 1 },
  { $set: { age: 29 } }
)
  
db.patient.updateOne( {patient_id : 1}, {$unset : {sex : ""}})

db.patient.update( {patient_id : 6}, {$unset : {weight: ""} })

db.patient.update( { patient_id: 1 }, { $inc: { age: 1 } } )


/* Update one document with upsert  */
/************************************/   

db.patient.updateOne(
  { patient_id : 1000 },
  { $set: { first_name : "first_name_1000", 
    last_name : "last_name_1000", 
    age: 62, height:162, weight:68} },
  { upsert: true }
);

/* Update many documents */
/*************************/  
    
db.patient.updateMany(
    {},
    { $inc: {age: 1} }
);

db.patient.updateMany(
    { age : {$gte : 50 }},
    { $inc: {age: 1} }
);

/* Select with conditions */
/**************************/  

db.patient.find( { sex: "F" } )
db.patient.find( { age: { $gte: 50} } )
db.patient.find( { sex : 'H', age: { $lt: 50} } )


db.patient.find().sort(age:-1)

db.patient.find({sex:{$exists:true}})


    
/* Vider une collection  */
/*************************/
    
db.patient.deleteMany({})
 
db.patient.deleteMany( { city : "Lille" } )

/*  Text search         */
/*************************/
 
db.stores.createIndex( { name: "text", description: "text" } )
 
// Search for any term of the list
db.stores.find( { $text: { $search: "java coffee shop" } } )
 
// search by exact term (between quotes)
db.stores.find( { $text: { $search: "\"coffee shop\"" } } )
 
// term exclusion (-coffee)
db.stores.find( { $text: { $search: "java shop -coffee" } } )

// sorting
db.stores.find(
   { $text: { $search: "java coffee shop" } },
   { score: { $meta: "textScore" } }
).sort( { score: { $meta: "textScore" } } )

/* Agregation            */
/*************************/

// Count
db.patient.count()

// Count with a condition
db.patient.count(
    { address : { $exists: true }}
)

//
db.patient.count( 
    { age: { $gt: 50 } } 
)

// Similar to
db.patient.find( { age: { $gt: 40 } } ).count()

// Count
db.patient.aggregate( [
   { $count: "nb_patient" }
])


/* Agregation            */
/*************************/

db.patient.aggregate([
   { $group: {_id: "$sex", 
              count: { $sum: 1 } } }
])

db.patient.aggregate([
   { $match: { adress : { $exists: true } } },
   { $group: {_id: "$sex", 
              age_min: { $min: "$age" },
              age_max: { $max: "$age" } } }
])

db.patient.aggregate([
   { $group: {_id: "$sex", 
              count: { $sum: 1 } } },
  { $sortByCount: count}
])

db.patient.aggregate( [

])


/* Cursor                */
/*************************/


/* MapReducec            */
/*************************/

db.patient.mapReduce( 
function(){ emit( this.sex, 1);},
function(key, values){return Array.sum(values)},
{
    query : {sex:{$exists:true}},
    out: "sex_count"
}).find()

db.patient.mapReduce( 
function(){ emit( this.address.city, 1);},
function(key, values){return Array.sum(values)},
{
    query : {"address.city":{$exists:true}},
    out: "city_count"
}).find()