
-- MariaDB / MySQL

drop table person;

create table person (
	person_id				integer not null,
	person_name				varchar(100),
	health_status 			varchar(50),	
	confirmed_status_date 	datetime,
	address_lat				decimal(15,10),
	address_long 			decimal(15,10),
	constraint primary key (person_id)
);

drop table place;

create table place (
	place_id	integer not null,
	place_name	varchar(100),
	place_type	varchar(15),
	constraint primary key (place_id)
);



create table visit (
	place_id	integer not null,
	place_name	varchar(100),
	place_type	varchar(15),
	constraint primary key (place_id)
);



truncate place;

truncate person;



LOAD DATA INFILE '/home/ant/Documents/00_Cours/courses/big_data/nosql_neo4j/tp/person_with_date_mariadb.csv'
INTO TABLE person
FIELDS TERMINATED BY ';'
LINES STARTING BY '"'
TERMINATED BY '"\r\n';