-- Select
-- -------------------------------------------------------------

-- Select all columns of PATIENT


-- Select first name and last name of PATIENT


-- Select the patients born after 1980


-- Count the number of rows in PATIENT


-- Count the number of patient per sex


-- Count the number of stay with admission after February 2020.


-- Count the number of stay per admission month.


-- Update
-- -------------------------------------------------------------

-- Update the birth date of patient 1. New birth_date = '2000-04-30'



-- Delete
-- -------------------------------------------------------------

-- Delete the patient born on 2099-01-01.



-- Insert
-------------------------------------------------------------

-- Insert a new patient FIRST_NAME_21, NAME_21, '2000-05-06'



-- Join
-- -------------------------------------------------------------

-- Select the admission and discharge date of each patient's stay



-- Count the number of stay per patient



-- Select the patient who have more than one stay.